import random

passLen = 10
numPass = 5
f = open('psw.txt', 'w')

alphabet = "aAbBcCdDeEfFgGhHiIkKlLmMnNoOpPqQrRsStTvVxXyYzZ0123456789"
alphabetLen = len(alphabet)




j = 0
passwords = []
while j < numPass:
    i = 0
    password = ""
    while i < passLen:
        numEl = random.randint(0, alphabetLen-1)
        password = password + alphabet[numEl]
        i = i + 1
    passwords.append(password)
    j = j + 1

for el in passwords:
    f.write(el + '\n')
    
f.close()
